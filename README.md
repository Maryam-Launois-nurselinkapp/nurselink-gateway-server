# Nurselink-gateway-server

### General informations

* This folder is about the configuration of Gateway server for Nurselink APIS.


* The server has its own Dockerfile. It will be launched by the Docker-compose file where is in [nurslink-docker](https://gitlab.com/Maryam-Launois-nurselinkapp/nurselink-docker) folder.


### Nurselik microservices architecture

![Nurselink architecture](Nurselink architecture.PNG)
