package com.lau.nurselink.gateway.server.nurselinkgatewayserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NurselinkGatewayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NurselinkGatewayServerApplication.class, args);
	}

}
