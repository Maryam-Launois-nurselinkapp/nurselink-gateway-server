FROM adoptopenjdk/openjdk11:latest

ARG JAR_FILE=target/*.jar

RUN mkdir /opt/gateway-server

COPY ${JAR_FILE} /opt/gateway-server/app.jar

ENTRYPOINT ["java", "-jar", "/opt/gateway-server/app.jar"]